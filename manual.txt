ZingMISC Instruction Set Manual
----------------------------
this document is licensed under CC BY-SA 4.0 
===============================

INSTRUCTIONS
--------------
SI := sets a register to a immediate
CAJ := jumpes to next label if CR is true
LAB := label
SR := sets a register to a register
GT := greater than
LT := less than
OR := or gate
NOT := not gate
AND := and gate
ADD := add register to register
SUB := subtract register from register
SWP := swaps bytes in register then copies it to register
=============

OPCODES
-------------
SI := [0x02] [RX] [IX]
CAJ := [0x03] [NULL] [NULL]
LAB := [0x04] [NULL] [NULL]
SR := [0x05] [RX] [RY]
GT := [0x07] [RX] [RY]
LT := [0x09] [RX] [RY]
OR := [0x08] [RX] [RY]
NOT := [0xA] [RX] [RY]
AND := [0xB] [RX] [RY]
ADD := [0xC] [RX] [RY]
SUB := [0XD] [RX] [RY]
SWP := [0x6] [RX] [RY]
=============

REGISTERS
--------------
general purpuse:
    EAX := 0xA1
    EBX := 0xB1
    ECX := 0xC1
    EDX := 0xD1
    AX := 0xA2
    BX := 0xB2
    CX := 0xC2
    DX := 0xD2
processor related:
    IP := 0x21 := instruction pointer
    CR := 0x22 := comparison register
pins:
    J1 := 0x31
    J2 := 0x32
    J3 := 0x33
    J4 := 0x34
    J5 := 0x35
    L1 := 0x41
    L2 := 0x42
    L3 := 0x43
    L4 := 0x44
    L5 := 0x45
pins for extended edition:
    E1 := 0x55
    B2 := 0x54
    E3 := 0x53
    E4 := 0x52
    C5 := 0x51
    U1 := 0x65
    U2 := 0x64
    Y3 := 0x63
    Z4 := 0x62
    Z5 := 0x61
you can add custom pins starting at byte range 0x7F
but you would need to modify your assembler
==============